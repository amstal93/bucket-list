<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = App\User::all();
        foreach ($users as $user) {

            $total = rand(3,4);

            for ($i=0; $i < $total; $i++) {
                $order = $i+1;
                $user->Categories()
                ->save(factory(App\Category::class)
                ->make(['order'=>$order]));
            }
        }
    }
}
