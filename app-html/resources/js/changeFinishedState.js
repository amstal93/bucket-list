document.addEventListener('DOMContentLoaded', function() {
  const checkBoxElements = Array.from(document.getElementsByClassName('change-finished-state'))
  const csrfToken = document.querySelector('meta[name="csrf-token"]').content

  checkBoxElements.forEach(element => {
    const liElement = element.closest('li')

    element.addEventListener(
      'change',
      function(e) {
        const finished = this.checked ? 1 : 0

        const xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function() {
          if (xhr.readyState === 4) {
            if (xhr.status === 200) {
              if (this.checked) {
                liElement.classList.add('finished')
              } else {
                liElement.classList.remove('finished')
              }
              window.vm.$refs.progressBar.getCount()
            } else {
              element.checked = !finished
            }
          }
        }
        xhr.open('PATCH', '/change-finished-state/' + this.dataset.id)
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
        xhr.setRequestHeader('X-CSRF-TOKEN', csrfToken)
        xhr.send('finished=' + finished)
      },
      { passive: true },
    )
  })
})
