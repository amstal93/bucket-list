<div class="modal fade" id="modal-addItem" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Add item</h5>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>

            <div class="modal-body">
                <form action="/store" id="modal-addItem-form" method="post">
                    @csrf
@if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
    @foreach ($error->addItem->all() as $error)
                            <li>{{ $error }}</li>
    @endforeach
                    </ul>
                </div>
@endif
                    <div class="form-group row">
                        <label for="add-item-title" class="col-sm-4 col-form-label">Title</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="add-item-title" name="title">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="add-item-note" class="col-sm-4 col-form-note">Note</label>
                        <div class="col-sm-8">
                            <textarea row="4" class="form-control" id="add-item-note" name="note"></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="add-item-category" class="col-sm-4 col-form-category">Category</label>
                        <div class="col-sm-8">
                            <select name="category_id" class="form-control" id="add-item-category">
                                <option value="0" selected>Uncategorized</option>
@foreach (Auth::user()->categories as $category)
                                <option value="{{ $category->id }}">{{ $category->title }}</option>
@endforeach
                            </select>
                        </div>
                    </div>

                </form>

            </div>

            <div class="modal-footer">
                <button type="submit" form="modal-addItem-form" class="btn btn-primary">Submit</button>
            </div>

        </div>
    </div>
</div>
