<li id="item-{{ $item->id }}" class="list-group-item d-flex justify-content-between align-items-center @if($item->finished) finished @endif" style="padding-left:2.5rem">
    <input class="form-check-input change-finished-state" type="checkbox" {{ $item->get_checked() }} id="item-{{ $item->id }}-finishedCheck" data-id="{{ $item->id }}">
    <label class="form-check-label" for="item-{{ $item->id }}-finishedCheck">
@if (!$trash_box)
        <span class="lead">{{ $item->order }}. </span>
@endif
        {{ $item->title }}
    </label>
    <button type="button" class="btn btn-secondary btn-sm rounded-pill" data-toggle="modal" data-target="#modal-editItem-{{ $item->id }}">edit</button>
    @include('parts.modal.editItem')
</li>
