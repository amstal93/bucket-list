<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

/** HomeController **/

Route::get('/', 'HomeController@index')->name('home');

Route::get('/trash', 'HomeController@trashBox')->name('trashBox');

Route::get('/get-count', 'HomeController@getCount')->name('getCount');

/** ItemController **/

Route::post('/store', 'ItemController@store')->name('itemStore');

Route::patch('/update/{item}', 'ItemController@update')
    ->where('item', '[0-9]+')->name('itemUpdate');

Route::patch('/change-finished-state/{item}', 'ItemController@changeFinishedState')
    ->where('item', '[0-9]+')->name('changeFinishedState');

Route::patch('/into-trash/{item}', 'ItemController@intoTrash')
    ->where('item', '[0-9]+')->name('intoTrash');

Route::patch('/back-from-trash/{item}', 'ItemController@backFromTrash')
    ->where('item', '[0-9]+')->name('backFromTrash');

Route::delete('/delete/{item}', 'ItemController@destroy')
    ->where('item', '[0-9]+')->name('itemDelete');

/** CategoryController **/

Route::post('/category/store', 'CategoryController@store')->name('categoryStore');

Route::patch('/category/update/{category}', 'CategoryController@update')
    ->where('category', '[0-9]+')->name('categoryUpdate');

Route::delete('/category/delete/{category}', 'CategoryController@destroy')
    ->where('category', '[0-9]+')->name('categoryDelete');
